$(document).ready(function () {
  $("#categoryTable").DataTable();
});

// ** =============== Add Category to <table>=============== //
function categoriesAdd() {
  if ($("#categoryTable tbody").length == 0) {
    $("#categoryTable").append("<tbody></tbody>");
  }
}

function categoryUpdate() {
  console.log("hello world");
  categoriesAddToTable();
  formClear();
}

function categoriesAddToTable() {
  if ($("#categoryTable tbody").length == 0) {
    $("#categoryTable").append("<tbody></tbody>");
  }

  $("#categoryTable tbody").append(
    "<tr>" +
      "<td>" +
      $("#categoryname").val() +
      "</td>" +
      "<td>" +
      $("#subcategoryname").find(":selected").text() +
      "</td>" +
      "<td>" +
      $("#qty").val() +
      "</td>" +
      "<td>" +
      $("#price").val() +
      "</td>" +
      "<td>" +
      "<button type='button' class='btn btn-danger' onclick='productDelete(this);'>" +
      "Delete" +
      "</button>" +
      "</td>" +
      "</tr>"
  );
}
function formClear() {
  $("#categoryname").val("");
  $("#subcategoryname").val("");
  $("#qty").val("");
  $("#price").val("");
}
function productDelete(ctl) {
  $(ctl).parents("tr").remove();
}
